package com.example.robson.sqlite;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Robson on 27.04.2017.
 */

public class PersonListAdapter extends ArrayAdapter<Person> {
    Context context;
    List<Person> people;

    public PersonListAdapter(Context context, List<Person> people) {
        super(context, -1, people);
        this.context = context;
        this.people = people;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null)
        {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            rowView = layoutInflater.inflate(R.layout.list_item, null);
        }

        TextView index = (TextView) rowView.findViewById(R.id.Index);
        TextView name = (TextView) rowView.findViewById(R.id.Name);
        TextView surname = (TextView) rowView.findViewById(R.id.Surname);
        TextView email = (TextView) rowView.findViewById(R.id.Email);

        index.setText("Id: " +  position);
        name.setText("Imię: " + String.valueOf(people.get(position).name));
        surname.setText("Nazwisko: " + String.valueOf(people.get(position).surname));
        email.setText("Email: " + String.valueOf(people.get(position).email));

        return rowView;
    }
}
