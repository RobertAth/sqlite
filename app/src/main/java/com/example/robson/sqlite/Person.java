package com.example.robson.sqlite;
/**
 * Created by Robson on 27.04.2017.
 */

public class Person {
    String name;
    String surname;
    String email;

    public Person(String name, String surname, String email) {
        this.name = name;
        this.surname = surname;
        this.email = email;
    }
}
