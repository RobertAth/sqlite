package com.example.robson.sqlite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button save;
    EditText name;
    EditText surname;
    EditText email;
    ListView list;
    List<Person> people = new ArrayList<>();
    PersonListAdapter adapter;

    long currentItemId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list = (ListView) findViewById(R.id.List);
        save = (Button) findViewById(R.id.Save);
        name = (EditText) findViewById(R.id.Name);
        surname = (EditText) findViewById(R.id.Surname);
        email = (EditText) findViewById(R.id.Email);
        adapter = new PersonListAdapter(this, people);
        list.setAdapter(adapter);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().equals("") || surname.getText().toString().equals("") || email.getText().toString().equals("")) {
                    return;
                }

                people.add(new Person(name.getText().toString(), surname.getText().toString(), email.getText().toString()));
                adapter.notifyDataSetChanged();

                name.setText("");
                surname.setText("");
                email.setText("");

                getWindow().getDecorView().clearFocus();
            }
        });
    }
}